import java.util.Scanner;
public class ApplianceStore
{
	public static void main(String[] args)
	{
		Scanner keyboard = new Scanner(System.in);
		Refrigerator[] fridge = new Refrigerator[4];
		for(int i = 0; i < fridge.length; i++)
		{
			fridge[i] = new Refrigerator();
			System.out.println("What is the brand of the Fridge?");
			fridge[i].brand = keyboard.next();
			System.out.println("How much voltage does the Fridge have?");
			fridge[i].voltage = keyboard.nextInt();
			System.out.println("How big is the Fridge length-wise?(in cm)");
			fridge[i].height = keyboard.nextDouble();
		}
		System.out.println(fridge[3].brand);
		System.out.println(fridge[3].voltage);
		System.out.println(fridge[3].height);
		fridge[0].dispenseWater();
		fridge[0].dispenseIce();
	}
}